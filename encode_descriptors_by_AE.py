import time
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from keras.models import model_from_json
from keras.layers import Input, Conv1D, MaxPooling1D, Flatten, Dense, UpSampling1D, ZeroPadding1D
from keras.models import Model
from keras.datasets import mnist
import keras
import os
import numpy as np
from slackIntegration import logSlack

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

if __name__ == '__main__':
    try:
        start = time.time()
        batch_size = 128
        epochs = 40
        # descriptor matrix calculation
        print("descriptor matrix calculation.....")
        with open("descriptors.txt") as textFile:
            lines = [line.split() for line in textFile]
        descriptors = [line[0].split(",") for line in lines]
        descriptor_matrix = [value[1:] for value in descriptors]
        descriptor_matrix = np.asarray(descriptor_matrix, dtype=int)
        descriptor_matrix = descriptor_matrix.reshape(len(descriptor_matrix),len(descriptor_matrix[0]),1)

        print("Building autoencoder....")
        # print(len(descriptor_matrix))
        print(len(descriptor_matrix[0]))
        # Convolutional Encoder
        shape = (len(descriptor_matrix[0]),1)
        input_M = Input(shape=shape)
        conv_1 = Conv1D(filters=2, kernel_size=3, activation='relu', padding='same')(input_M)
        pool_1 = MaxPooling1D(2, padding='same')(conv_1)
        # conv_2 = Conv1D(4, 3, activation='relu', padding='same')(pool_1)
        # pool_2 = MaxPooling1D(2, padding='same')(conv_2)
        # conv_3 = Conv1D(2, 3, activation='relu', padding='same')(pool_2)
        # pool_3 = MaxPooling1D(2, padding='same')(conv_3)
        conv_4 = Conv1D(1, 3, activation="relu", padding = 'same')(pool_1)  
        encoded = MaxPooling1D(2, padding='same', name='bottleneck')(conv_4)     
        
         # Decoder
        conv_5 = Conv1D(1, 3, activation="relu", padding = 'same')(encoded)
        up_1 = UpSampling1D(2)(conv_5)
        conv_6 = Conv1D(2, 3, activation='relu', padding='same')(up_1)
        # up_2 = UpSampling1D(2)(conv_6)
        # conv_7 = Conv1D(4, 3, activation='relu', padding='same')(up_2)
        # up_3 = UpSampling1D(2)(conv_7)
        # conv_8 = Conv1D(8, 3, activation='relu',  padding='same')(up_3)
        up_4 = UpSampling1D(2)(conv_6)
        # up_padd = ZeroPadding2D(padding=(1, 2))(up_3)
        decoded = Conv1D(1, 3, activation='sigmoid',
                        padding='same', name='autoencoder')(up_4)
    
        model = Model(inputs=input_M, outputs=[decoded])

        model.compile(loss='binary_crossentropy',
                    optimizer='adam',
                    )
        model.summary()
        print("Start training.....")
        history = model.fit(descriptor_matrix,
                {'autoencoder': descriptor_matrix},
                batch_size=batch_size,
                epochs=epochs,
                verbose=1)

        # # Serialize model to json
        model_json = model.to_json()
        with open("model.json", "w") as json_file:
            json_file.write(model_json)
        # serialize weights to HDF5
        #model.save_weights("model.h5")
        #print("Model saved to disk")
        # Plot training & validation accuracy values
       # plt.plot(history.history['acc'])
        # plt.plot(history.history['val_acc'])
        plt.title('Model accuracy')
        plt.ylabel('Accuracy')
        plt.xlabel('Epoch')
        plt.legend(['Train', 'Test'], loc='upper left')
        plt.show()

        # Plot training & validation loss values
        plt.plot(history.history['loss'])
        # plt.plot(history.history['val_loss'])
        plt.title('Model loss')
        plt.ylabel('Loss')
        plt.xlabel('Epoch')
        plt.legend(['Train', 'Test'], loc='upper left')
        plt.show()

        execTime= time.time() - start
        logSlack("Done | time {execTime}".format(execTime=execTime))
        print("Done | time {execTime}".format(execTime=execTime))
    except Exception as e:
        logSlack(str(e))
        raise
