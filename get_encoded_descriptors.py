import keras
import os
import numpy as np 
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
from keras.models import Model
from keras.layers import Input, Conv2D, MaxPooling2D, Flatten, Dense, UpSampling2D, ZeroPadding2D
from keras.models import model_from_json
from keras.utils import plot_model
from slackIntegration import logSlack



try:
    # descriptor matrix calculation
    print("Descriptor matrix calculation.....")
    with open("descriptors.txt") as textFile:
        lines = [line.split() for line in textFile]
    descriptors = [line[0].split(",") for line in lines]
    descriptor_matrix = [value[1:] for value in descriptors]
    descriptor_matrix = np.asarray(descriptor_matrix, dtype=int)
    descriptor_matrix = descriptor_matrix.reshape(len(descriptor_matrix),len(descriptor_matrix[0]),1)

    #load model
    json_file = open('model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights("model.h5")
    print("Loaded model from disk")
    loaded_model.compile(loss='binary_crossentropy',
                        optimizer='adam',
                        )
    loaded_model.summary()
    plot_model(loaded_model, to_file='model.png')
    encoder = Model(loaded_model.input, loaded_model.get_layer('bottleneck').output)                
    encoded_predicted_data = encoder.predict(descriptor_matrix)
    for descriptor, predicted_data  in zip(descriptors, encoded_predicted_data):
        with open("./AIA_Images/{descriptor}.txt".format(descriptor=descriptor[0]), "a") as file:
            enc_desc_str = ','.join(str(x[0]) for x in predicted_data)
            file.write(enc_desc_str)
            print("./AIA_Images/{descriptor}.txt".format(descriptor=descriptor[0]))
    print("Done")
    logSlack("Done")
except Exception as e:
        logSlack(str(e))
        raise