from imutils import contours
from skimage import measure
import numpy as np
import argparse
import imutils
import cv2
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import time
import os
from multiprocessing import Pool, freeze_support  # as ThreadPool
from slackIntegration import logSlack

imagePaths = ["./AIA_Images/{x}".format(x=x)
                                        for x in os.listdir("./AIA_Images/") if x.endswith(".jpg")]
image_path = "./AIA_Images/2012_1_1_T_0_0_0.jpg"


def detect_bright_regions(image_path):
    # load the image, convert it to grayscale, and blur it
    image = cv2.imread(image_path)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (11, 11), 0)
    thresh = cv2.threshold(blurred, 180, 255, cv2.THRESH_BINARY)[1]
    thresh = cv2.erode(thresh, None, iterations=2)
    thresh = cv2.dilate(thresh, None, iterations=4)
    return thresh


def calculate_descriptor(detected_bright_regions_image):
    img_height_vec = [0] * detected_bright_regions_image.shape[0]
    img_width_vec = [0] * detected_bright_regions_image.shape[1]
    for i in range(0, detected_bright_regions_image.shape[0]):
        for j in range(0, detected_bright_regions_image.shape[1]):
            if(detected_bright_regions_image[i][j] > 180):
                img_height_vec[i] = 1
                img_width_vec[j] = 1    
    return np.concatenate((img_height_vec, img_width_vec))


def thread_calculate_description(img_path):
    start_time = time.time()
    detected_bright_regions_image = detect_bright_regions(img_path)
    descriptor = calculate_descriptor(detected_bright_regions_image)
    with open("descriptors.txt", "a") as file:
        desc_str = ','.join(str(x) for x in descriptor)
        file.write("{img_path},{desc}\n".format(img_path=os.path.basename(img_path), desc=desc_str))
    computation_time = time.time() - start_time
    print("Descriptor calculated in {comp_time} sec.".format(
        comp_time=computation_time))

if __name__ == '__main__':
    try:
        # detected_bright_regions_image = detect_bright_regions(image_path)
        # plt.imsave("thresh.jpg", detected_bright_regions_image)
        start = time.time()
        freeze_support()
        pool = Pool(8)
        pool.map(thread_calculate_description, imagePaths)
        # for path in imagePaths:
        #     thread_calculate_description(path)
        execTime= time.time() - start
        logSlack("Done | time {execTime}".format(execTime=execTime))
        print("Done")
    except Exception as e:
        logSlack(str(e))
        raise



# np.savetxt("height.txt", [descriptor[0]], delimiter="\n", fmt='%d')
# np.savetxt("width.txt", [descriptor[1]], delimiter="\n", fmt='%d')

# plt.imsave("thresh.jpg", detected_bright_regions_image)
# plt.show()

# plt.imshow(detected_bright_regions_image)
# plt.show()
